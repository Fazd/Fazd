<!DOCTYPE html>
<html>
	<head>
		<!-- Scripts -->
		<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
		<script src="/inc/js/app.js"></script>
		<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
		<script type="text/javascript" src="/inc/js/materialize.min.js"></script>

		<!-- CSS -->
		<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<link type="text/css" rel="stylesheet" href="/inc/css/materialize.min.css"  media="screen,projection"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	</head>
	<body>

		<div ng-app="fazdApp">

			<nav>
				<div class="nav-wrapper">
					<a href="#" class="brand-logo" style="padding-left: 6px">Fazd</a>
					<ul id="nav-mobile" class="right hide-on-med-and-down">
						<li><a>Recipes</a></li>
						<li><a>Ingredients</a></li>
						<li><a>Etc.</a></li>
					</ul>
				</div>
			</nav>


			<div ng-controller="recipeController" class="container">
				<h2>Recipe Management</h2>

				<h5>Add Recipe</h5>
				<form>

					<div class="row">
						<div class="input-field col s12">
							<label for="recipeName">Name</label>
							<input id="recipeName" type="text" ng-model="newRecipe.name"></input>
						</div>
					</div>

					<div class="row">
						<div class="input-field col s12">
							<label for="recipeIngredients">Ingredients</label>
							<input id="recipeIngredients" type="text" ng-model="newRecipe.ingredients"></input>
						</div>
					</div>

					<div class="row">
						<div class="input-field col s5">
							<label for="recipeCookMins">Cook Minutes</label>
							<input id="recipeCookMins" type="text" ng-model="newRecipe.cookMins"></input>
						</div>

						<div class="input-field col s5">
							<label for="recipePrepareMins">Prepare Mins</label>
							<input id="recipePrepareMins" type="text" ng-model="newRecipe.prepareMins"></input>
						</div>

						<div class="col s2">
							<button class="btn" type="button" ng-click="addRecipe()">Add</button>
						</div>
					</div>
					<br/>
				</form>

				<h5>Recipe List</h5>

				<table class="table striped">
					<thead><th>ID</th><th>Name</th><th>Ingredients</th><th>Cook Minutes</th><th>Prepare Minutes</th><th>Actions</th></thead>
					<tr ng-repeat="recipe in recipes">
						<td>{{ recipe.id }}</td>
						<td>{{ recipe.name }}</td>
						<td>{{ recipe.ingredients }}</td>
						<td>{{ recipe.cookMins }}</td>
						<td>{{ recipe.prepareMins }}</td>
						<td><button class="btn-flat" type="button" ng-click="deleteThis(recipe.id)"><i class="material-icons">delete</i></td>
					</table>

				</div>

			</div>

		</body>
	</html>
