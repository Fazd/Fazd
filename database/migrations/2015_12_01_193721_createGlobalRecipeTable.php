<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGlobalRecipeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('globalRecipes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('ingredients');
            $table->integer('cookMins');
            $table->integer('prepareMins');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('globalRecipes');
    }
}
