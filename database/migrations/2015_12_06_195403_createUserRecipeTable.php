<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserRecipeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('userRecipe', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('userRecipeList')->unsigned();
            $table->foreign('userRecipeList')->references('id')->on('userRecipeList');
            $table->string('name');
            $table->string('ingredients');
            $table->integer('cookMins');
            $table->integer('prepareMins');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('userRecipe');
    }
}
