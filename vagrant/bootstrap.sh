#!/usr/bin/env bash

# noninteractive mode for installing packages and resolving config conflicts
export DEBIAN_FRONTEND=noninteractive

provision() {
    locale-gen en_GB.UTF-8
    apt-get -qq update
    apt-get -y install vim curl build-essential python-software-properties git > /dev/null 2>&1
}

provision_mysql() {
    # MySQL
    debconf-set-selections <<< 'mysql-server mysql-server/root_password password vagrant'
    debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password vagrant'
    apt-get -y install mysql-server > /dev/null 2>&1

    # Configure MySQL to bind to 0.0.0.0
    sed -i "s/bind-address.*/bind-address = 0.0.0.0/" /etc/mysql/my.cnf

    # Add grants to allow connecting to MySQL without SSH
    mysql -uroot -pvagrant -e "use mysql; GRANT ALL ON *.* to root@'%' IDENTIFIED BY 'vagrant'; FLUSH PRIVILEGES;"
    service mysql restart
}

provision_php() {

    # PHP
    apt-get -y install php5 php5-fpm php5-curl php5-gd php5-mcrypt php5-mysql php-apc php5-cli php5-xdebug > /dev/null 2>&1
    php5enmod apcu curl gd json mcrypt mysqli mysql opcache pdo pdo_mysql readline xdebug

    service php5-fpm restart
}

provision_nginx() {

    # Nginx
    apt-get -y install nginx

    # symlink the app as the web directory
    if ! [ -L /var/www ]; then
        rm -rf /var/www
        ln -fs /vagrant /var/www
    fi

    cp -f /vagrant/vagrant/etc/nginx/default /etc/nginx/sites-available/default

    service nginx restart
}


provision
provision_mysql
provision_php
provision_nginx
