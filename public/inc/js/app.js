(function() {
	var app = angular.module("fazdApp", []);

	app.controller("recipeController", ['$scope', '$http', function($scope, $http) {

		$scope.newRecipe = {
			"name": "",
			"ingredients": "",
			"cookMins": 0,
			"prepareMins": 0
		};

		$scope.getRecipes = function() {
			$http.get('/global_recipes').then(function(data) {
				$scope.recipes = data.data;
			});
		};

		$scope.addRecipe = function(recipe) {
			$http.post('/global_recipes', $scope.newRecipe).then(function() {
				$scope.getRecipes();
			});
		};

		$scope.deleteThis = function(id) {
			$http.delete('/global_recipes/'+id).then(function() {
				$scope.getRecipes();
			});
		};

		$scope.getRecipes();
	}]);

})();
