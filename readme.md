Fazd Food.

Fazd is a recipe recommendation engine.


#Info for Alex..

##Protected routes

There is a login route at auth/login that accepts an email and password field and will return a JWT token. (a SHA256 hash).

In order to be able to use the parts of the API that require authentication, you will have to set an authorization header as follows:

Authorization: Bearer {yourtokenhere}
Now in your methods that require authentication you can access the authenticated user using:

$user = JWTAuth::parseToken()->authenticate();

##Using Composer
If you get errors or have just done a git pull run compser install or composer update if it asks you to so you install the new dependencies.

If you need to add a package run composer require <package name>

