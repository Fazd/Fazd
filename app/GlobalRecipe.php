<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GlobalRecipe extends Model {
    /**
     * The database table name
     * @var string
     */
    protected $table = 'globalRecipes';

    /**
     * The mass assignable fields
     * @var array
     */
    protected $fillable = [''];
}
