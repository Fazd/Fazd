<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
//User Auth routes
$app->post('auth/login', 'AuthController@postLogin');
$app->post('auth/create', 'AuthController@storeUser');

//Routes which do not require authorization
$app->get('/', function() {
    return view('index');
});
$app->get('global_recipes', 'GlobalRecipeController@index');
$app->get('global_recipes/{id}', 'GlobalRecipeController@get');


//Routes which require authorization
$app->group(['middleware' => 'jwt.auth', 'namespace' => 'App\Http\Controllers'], function($app) {
    $app->post('global_recipes', 'GlobalRecipeController@store');
    $app->put('global_recipes/{id}', 'GlobalRecipeController@update');
    $app->delete('global_recipes/{id}', 'GlobalRecipeController@delete');

    $app->get('user_recipes', 'UserRecipeController@index');
    $app->get('user_recipes/{id}', 'UserRecipeController@get');
    $app->post('user_recipes', 'UserRecipeController@store');
    $app->put('user_recipes/{id}', 'UserRecipeController@update');
    $app->delete('user_recipes/{id}', 'UserRecipeController@delete');

    $app->get('user_recipe_lists', 'UserRecipeListController@index');
    $app->get('user_recipe_lists/{id}', 'UserRecipeListController@get');
    $app->post('user_recipe_lists', 'UserRecipeListController@store');
    $app->put('user_recipe_lists/{id}', 'UserRecipeListController@update');
    $app->delete('user_recipe_lists/{id}', 'UserRecipeListController@delete');
});
