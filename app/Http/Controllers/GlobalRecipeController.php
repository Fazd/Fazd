<?php

namespace App\Http\Controllers;

use App\GlobalRecipe;
use Illuminate\Http\Request;

class GlobalRecipeController extends Controller
{
    /**
     * Return all global recipes
     *
     * @return Json
     */
    public function index()
    {
        return response()->json(GlobalRecipe::all(), 200);
    }

    /**
     * Store a global recipe
     *
     * @param Request $request
     * @return Json|Response
     */
    public function store(Request $request)
    {
        $recipeModel = new GlobalRecipe;

        $recipeModel->name = $request->input('name');
        $recipeModel->ingredients = $request->input('ingredients');
        $recipeModel->cookMins = $request->input('cookMins');
        $recipeModel->prepareMins = $request->input('prepareMins');

        if ($recipeModel->save()) {
            return response()->json($recipeModel, 201);
        }

        return response('', 202);
    }

    /**
     * Update a global recipe
     *
     * @param Request $request
     * @param int $id the ID of the recipe to update
     * @return Json|Reponse
     */
    public function update(Request $request, $id)
    {
        $recipeModel = GlobalRecipe::find($id);

        if (!$recipeModel) {
            return response('', 204);
        }

        $recipeModel->name = $request->input('name');
        $recipeModel->ingredients = $request->input('ingredients');
        $recipeModel->cookMins = $request->input('cookMins');
        $recipeModel->prepareMins = $request->input('prepareMins');

        if ($recipeModel->save()) {
            return response()->json($recipeModel, 200);
        }

        return response('', 202);
    }

    /**
     * Return a global recipe by id
     *
     * @param int $id The id of the recipe to return
     * @return Json|Response
     */
    public function get($id)
    {
        $recipeModel = GlobalRecipe::find($id);

        if ($recipeModel) {
            return response()->json($recipeModel, 200);
        }

        return response('', 204);
    }

    /**
     * Remove a global recipe by id
     *
     * @param int $id The id of the recipe to remove
     * @return Json|Response
     */
    public function delete($id)
    {
        $recipeModel = GlobalRecipe::find($id);

        if ($recipeModel->delete()) {
            return reponse('', 200);
        }

        return response('', 204);
    }
}
