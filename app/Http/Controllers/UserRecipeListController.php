<?php

namespace App\Http\Controllers;

use App\UserRecipeList;
use JWTAuth;
use Illuminate\Http\Request;

class UserRecipeListController extends Controller
{
    /**
     * Return all user recipe lists
     *
     * @return Json
     */
    public function index()
    {
        return response()->json(UserRecipeList::all(), 200);
    }

    /**
     * Store a user recipe list
     *
     * @param Request $request
     * @return Json|Response
     */
    public function store(Request $request)
    {
        $recipeModel = new UserRecipeList;
        $userModel = JWTAuth::parseToken()->authenticate();
        $recipeModel->userId = $userModel->id;

        if ($recipeModel->save()) {
            return response()->json($recipeModel, 201);
        }

        return response('', 202);
    }

    /**
     * Update a user recipe list
     *
     * @param Request $request
     * @param int $id the ID of the recipe list to update
     * @return Json|Reponse
     */
    public function update(Request $request, $id)
    {
        $recipeModel = UserRecipeList::find($id);

        if (!$recipeModel) {
            return response('', 204);
        }

        $recipeModel->name = $request->input('name');
        $userModel = JWTAuth::parseToken()->authenticate();

        if ($recipeModel->save()) {
            return response()->json($recipeModel, 200);
        }

        return response('', 202);
    }

    /**
     * Return a user recipe list by id
     *
     * @param int $id The id of the recipe list to return
     * @return Json|Response
     */
    public function get($id)
    {
        $recipeModel = UserRecipeList::find($id);

        if ($recipeModel) {
            return response()->json($recipeModel, 200);
        }

        return response('', 204);
    }

    /**
     * Remove a user recipe list by id
     *
     * @param int $id The id of the recipe list to remove
     * @return Json|Response
     *
     * TODO: Deleting list removes child recipes
     */
    public function delete($id)
    {
        $recipeModel = UserRecipeList::find($id);

        if ($recipeModel->delete()) {
            return reponse('', 200);
        }

        return response('', 204);
    }
}
