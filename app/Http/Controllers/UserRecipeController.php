<?php

namespace App\Http\Controllers;

use App\UserRecipe;
use Illuminate\Http\Request;

class UserRecipeController extends Controller
{
    /**
     * Return all user recipes
     *
     * @return Json
     */
    public function index()
    {
        return response()->json(UserRecipe::all(), 200);
    }

    /**
     * Store a user recipe
     *
     * @param Request $request
     * @return Json|Response
     */
    public function store(Request $request)
    {
        $recipeModel = new UserRecipe;
        $recipeModel->userRecipeList = $request->input('userRecipeList');
        $recipeModel->name = $request->input('name');
        $recipeModel->ingredients = $request->input('ingredients');
        $recipeModel->cookMins = $request->input('cookMins');
        $recipeModel->prepareMins = $request->input('prepareMins');

        if ($recipeModel->save()) {
            return response()->json($recipeModel, 201);
        }

        return response('', 202);
    }

    /**
     * Update a user recipe
     *
     * @param Request $request
     * @param int $id the ID of the recipe to update
     * @return Json|Reponse
     */
    public function update(Request $request, $id)
    {
        $recipeModel = UserRecipe::find($id);

        if (!$recipeModel) {
            return response('', 204);
        }

        $recipeModel->name = $request->input('name');
        $recipeModel->userRecipeList = $request->input('userRecipeList');
        $recipeModel->ingredients = $request->input('ingredients');
        $recipeModel->cookMins = $request->input('cookMins');
        $recipeModel->prepareMins = $request->input('prepareMins');

        if ($recipeModel->save()) {
            return response()->json($recipeModel, 200);
        }

        return response('', 202);
    }

    /**
     * Return a user recipe by id
     *
     * @param int $id The id of the recipe to return
     * @return Json|Response
     */
    public function get($id)
    {
        $recipeModel = UserRecipe::find($id);

        if ($recipeModel) {
            return response()->json($recipeModel, 200);
        }

        return response('', 204);
    }

    /**
     * Remove a user recipe by id
     *
     * @param int $id The id of the recipe to remove
     * @return Json|Response
     */
    public function delete($id)
    {
        $recipeModel = UserRecipe::find($id);

        if ($recipeModel->delete()) {
            return reponse('', 200);
        }

        return response('', 204);
    }
}
