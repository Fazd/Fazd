<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRecipe extends Model
{
    /**
     * The database table name
     * @var string
     */
    protected $table = 'userRecipe';

    /**
     * The mass assignable fields
     * @var array
     */
    protected $fillable = [];
}
