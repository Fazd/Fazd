<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRecipeList extends Model
{
    /**
     * The database table name
     * @var string
     */
    protected $table = 'userRecipeList';

    /**
     * The mass assignable fields
     * @var array
     */
    protected $fillable = [];
}
